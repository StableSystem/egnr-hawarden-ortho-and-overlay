# EGNR-Hawarden Ortho and Overlay

Optional ortho and overlay for EGNR-Hawarden by Zero Dollar Payware

Download the airport scenery from https://gitlab.com/StableSystem/egnr-hawarden/-/releases

**Installation Instructions:**
Download and move "EGNR-Hawarden", "yEGNR-Hawarden-Overlay", and "zEGNR-Hawarden-Ortho" folders into your custom scenery folder

Use xOrganizer or delete your scenery_packs.ini to adjust your scenery load order. If you have issues with the scenery loading most likely your load order is wrong or you are missing the RA library. 


Please report any bugs on the Zero Dollar Payware discord or on the official forum thread (linked above)

To conctact us about this repo please send a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-stablesystem/)

This repository and it's contents are protected under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
Assets used from other developers was done so with their knowledge and approval. 


**Citations/Credits**

World Imagery (orthophotos)

Esri. "Imagery" [basemap]. Scale Not Given. "World Imagery". January 21, 2021. https://www.arcgis.com/home/item.html?id=10df2279f9684e4a9f6a7f08febac2a9. (February 18, 2021).

MisterX Library

RA Library

SAM Library

iniBuilds

ZDP Developers: StableSystem, awsfiu, TJ